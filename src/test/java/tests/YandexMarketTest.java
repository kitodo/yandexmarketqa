package tests;

import com.codeborne.selenide.ex.ElementNotFound;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import pages.MarketProduct;
import pages.YandexMarket;
import pages.YandexSearch;

import static com.codeborne.selenide.Selenide.*;

public class YandexMarketTest {

    private YandexMarket yandexMarket;
    private YandexSearch yandexSearch;
    private MarketProduct marketProduct;
    private String minPrice = "1000";
    private String maxPrice = "2000";

    @Before
    public void Init() {
        yandexMarket = new YandexMarket();
        yandexSearch = new YandexSearch();
        marketProduct = new MarketProduct();
    }

    /** Через сервис "Поиск Янедекс" переходим к "Яндекс Маркет"
     * выполняем поиск товара в нужном ценовом диапазоне, переход междву вкладками,
     * проверка наименования, сортировка по цене и переход в 3-й магазин */


    @Test
    public void MainTest() {
        open("https://yandex.ru/");

        yandexSearch.search("market yandex");
        yandexSearch.clickButton("//button[./span[text()= \"Нет, спасибо\"]]");
        //sleep(10000);
        //Появляются ссылки с разным текстом, возможно из-за A/B тестирования.
        try {
            yandexSearch.clickLinkText(" — выбор и покупка товаров из...");
        }catch (ElementNotFound enf){
            yandexSearch.clickLinkText(" — выбор товаров и места их покупки");
        }


        switchTo().window(1);
        $(By.xpath("//button[./span[text()='Да, спасибо']]")).click();
        yandexMarket.searchYM("Xiaomi Redmi Power Bank Fast Charge 20000");
        yandexMarket.setMinPrice(minPrice);
        yandexMarket.setMaxPrice(maxPrice);

        yandexMarket.clickOnProduct("//div[1]/h3/a/span[1]");

        switchTo().window(2);

        marketProduct.checkText("//h1[text()='Аккумулятор Xiaomi Redmi Power Bank Fast Charge 20000']",
                "Аккумулятор Xiaomi Redmi Power Bank Fast Charge 20000");

        marketProduct.clickButton("//span[text()='Характеристики']");

        marketProduct.checkText("//h2[text()='О товаре']", "О товаре");
        sleep(2000);
        marketProduct.clickButton("//a[./span[text()=\"Цены\"]]");

        marketProduct.scrollTo("//div[@class='_1vFSS76Axn'][3]");
        marketProduct.clickButton("(//span[text()=\"Условия покупки: \"])[3]");
        marketProduct.clickButton("//div[@class='_36RTa1Qjye']");
        switchTo().window(3);
        sleep(10000);


    }


}
